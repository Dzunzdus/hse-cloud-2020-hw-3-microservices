
import os
from common.settings import datafile, SEARCH_DOCUMENTS_DATA_FILES
from common.data_source import CSV
from search.shards.service import SearchInShardsService
from search.simple.service import SimpleSearchServiceClient
from common.settings import SHARDS_NUM


def main():
    shards = []
    for i in range(1, SHARDS_NUM + 1):
        port_num = 3000 + i
        shards.append(SimpleSearchServiceClient('http://simple-search-{}:{}'.format(i, port_num)))

    search_in_shards = SearchInShardsService(shards)
    search_in_shards.run_server(4000, debug=True)


if __name__ == "__main__":
    main()
