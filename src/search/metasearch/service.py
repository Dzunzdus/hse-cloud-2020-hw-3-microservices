from typing import List, Dict

from geo.service import GeoServiceClient
from search.simple.service import BaseSearchServiceClient
from user.service import UserServiceClient


class MetaSearchService:
    def __init__(self,
                 search_in_shards_client: BaseSearchServiceClient,
                 user_service_client: UserServiceClient,
                 geo_service_client: GeoServiceClient):
        self._search_in_shards_client = search_in_shards_client
        self._user_service_client = user_service_client
        self._geo_service_client = geo_service_client

    def search(self, search_text, user_id, ip, limit=10) -> List[Dict]:
        user_data = self._user_service_client.get_user_data(user_id)  # {'gender': ..., 'age': ...}
        geo_data = self._geo_service_client.get_geo_data(ip)  # {'region': ...}
        df = self._search_in_shards_client.get_search_data(search_text, user_data, geo_data, limit)
        return df[self._search_in_shards_client.DOCS_COLUMNS].to_dict('records')
