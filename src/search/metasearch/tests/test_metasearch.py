import pytest

from common.data_source import CSV
from geo.service import GeoService, GeoServiceClient
from metasearch.service import MetaSearchService
from search.simple.service import SimpleSearchService, SimpleSearchServiceClient
from search.shards.service import SearchInShardsServiceClient, SearchInShardsService
from common.settings import USER_DATA_FILE, GEO_DATA_FILE, SEARCH_DOCUMENTS_DATA_FILES
from user.service import UserService, UserServiceClient
from mock import MagicMock


def metasearch(search_text, user_id, ip, limit):
    tmp_user_service = UserService(CSV(USER_DATA_FILE))
    tmp_geo_service = GeoService(CSV(GEO_DATA_FILE))

    mock_geo_data = tmp_geo_service.get_geo_data(ip)
    mock_user_data = tmp_user_service.get_user_data(user_id)

    user_service_client = UserServiceClient('')
    user_service_client.get_user_data = MagicMock(return_value=mock_user_data)

    geo_service_client = GeoServiceClient('')
    geo_service_client.get_geo_data = MagicMock(return_value=mock_geo_data)

    shards = []
    for file in SEARCH_DOCUMENTS_DATA_FILES:
        tmp_simple_search = SimpleSearchService(CSV(file))
        mock_simple_search_data = tmp_simple_search.get_search_data(search_text, mock_user_data, mock_geo_data, limit)
        shard = SimpleSearchServiceClient('')
        shard.get_search_data = MagicMock(return_value=mock_simple_search_data)
        shards.append(shard)

    search_in_shards = SearchInShardsService(shards=shards)
    search_in_shards_client = SearchInShardsServiceClient('')

    mock_shards_data = search_in_shards.get_search_data(search_text, mock_user_data, mock_geo_data, limit)
    search_in_shards_client.get_search_data = MagicMock(return_value=mock_shards_data)

    return MetaSearchService(search_in_shards_client, user_service_client, geo_service_client)


def test_integration_works():
    res = metasearch(search_text='politician', user_id=25, ip='3.103.8.10', limit=5).search(None, None, None)
    result_keys = [d['key'] for d in res]
    expected_keys = [
        'Dutch crackdown risks hurting mainstream Muslims',
        'Dutch Filmmaker Murder Suspect Faces Terror Charges',
        'Surprise victory for Basescu in Romania',
        'BLAIR PEACE HOPES',
        'France Opens Judicial Inquiry Into Holocaust Doubter'
    ]
    assert result_keys == expected_keys


def test_integration_bad_parameters():
    metasearch(search_text='politician', user_id=None, ip='0.0.0.0', limit=10).search(None, None, None)
    metasearch(search_text='politician', user_id=None, ip=None, limit=1).search(None, None, None)
    metasearch(search_text=None, user_id=None, ip=None, limit=1).search(None, None, None)
    metasearch(search_text='', user_id=None, ip=None, limit=1).search(None, None, None)
