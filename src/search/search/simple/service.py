from flask import Flask, request
import requests
import json
import pandas as pd
from common.data_source import AbstractDataSource
from abc import abstractmethod

import sys


def stupid_count_tokens(tokens, text):
    res = 0
    for token in tokens:
        if token in text:
            res += 1
    return res


class BaseSearchServiceClient:
    DOCS_COLUMNS = ['document', 'key', 'key_md5']

    @abstractmethod
    def get_search_data(self, search_text, user_data=None, geo_data=None, limit=10):
        pass


class SimpleSearchServiceClient(BaseSearchServiceClient):
    def __init__(self, base_url):
        self._baseurl = base_url

    def get_search_data(self, search_text, user_data=None, geo_data=None, limit=10):
        params = {'text': search_text,
                  'user_data': json.dumps(user_data),
                  'geo_data': json.dumps(geo_data),
                  'limit': limit}
        res = requests.get(self._baseurl + "/search", params=params)
        print("Simple search client: ", res.status_code, file=sys.stderr)
        try:
            decoded_res = res.json()
            if 'res' not in decoded_res:
                return None
            pd_dict = json.loads(decoded_res['res'])
            res = pd.DataFrame.from_dict(pd_dict)
            return res
        except ValueError as e:
            print("Got an exception: ", str(e), file=sys.stderr)
            return None


class SimpleSearchService(Flask):
    _data: pd.DataFrame

    def __init__(self, data_source: AbstractDataSource):
        self._DOCS_COLUMNS = ['document', 'key', 'key_md5']
        self._data = pd.DataFrame(
            data_source.read_data(),
            columns=[*self._DOCS_COLUMNS, 'gender', 'age_from', 'age_to', 'region']
        )
        super().__init__('simple-search')

        urls = [
            ('/search', self.search, {}),
        ]
        for url in urls:
            if len(url) == 3:
                self.add_url_rule(url[0], url[1].__name__, url[1], **url[2])

    def _build_tokens_count(self, search_text):
        tokens = search_text.split()
        res = self._data['document'].apply(lambda x: stupid_count_tokens(tokens, x))
        res.name = None
        return res

    def _get_geo_mask(self, geo_data=None):
        gd = geo_data.get('region') if geo_data is not None else None
        return self._data['region'] == gd

    def _get_gender_mask(self, user_data=None):
        ud = user_data.get('gender', 'null') if user_data is not None else 'non-existing gender'
        return self._data['gender'].apply(lambda x: stupid_count_tokens([ud], x))

    def _get_age_mask(self, user_data=None):
        user_age = int(user_data['age']) if user_data is not None else -1
        return self._data.apply(lambda x: x['age_from'] <= user_age <= x['age_to'], axis=1)

    def _sort_by_rating_and_tokens(self, rating, tokens_count, key_md5):
        df = pd.concat([tokens_count, rating, key_md5], axis=1)
        return df.sort_values([0, 1, 'key_md5'], ascending=[False, False, False])

    @staticmethod
    def _wrap_resp(df):
        return {"res": json.dumps(df.to_dict())}

    def search(self):
        search_text = request.args.get("text")
        user_data_encoded = request.args.get("user_data")
        user_data = json.loads(user_data_encoded)
        geo_data_encoded = request.args.get("geo_data")
        geo_data = json.loads(geo_data_encoded)
        limit_str = request.args.get("limit")
        if limit_str == "":
            limit_str = "10"
        try:
            limit = int(limit_str)
        except ValueError:
            return {"error": "can't convert limit"}
        return self._wrap_resp(self.get_search_data(search_text, user_data, geo_data, limit))

    @abstractmethod
    def get_search_data(self, search_text, user_data=None, geo_data=None, limit=10):
        # this is some simple algorithm that came to my mind, does not need to be useful or good, just something working
        if search_text is None or search_text == '':
            return pd.DataFrame([], columns=self._DOCS_COLUMNS)

        print("Get search data simple", file=sys.stderr)
        tokens_count = self._build_tokens_count(search_text)
        geo_mask = self._get_geo_mask(geo_data)
        gender_mask = self._get_gender_mask(user_data)
        age_mask = self._get_age_mask(user_data)
        rating = geo_mask + gender_mask + age_mask
        df = self._sort_by_rating_and_tokens(rating, tokens_count, self._data['key_md5'])
        return self._data.loc[df.head(limit).index]

    def run_server(self, port, **kwargs):
        print("PORT!!!: ", port, file=sys.stderr)
        super().run(host='0.0.0.0', port=port, **kwargs)