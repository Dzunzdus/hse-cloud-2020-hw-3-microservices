from common.data_source import AbstractDataSource
from flask import Flask, request
import requests
import sys


class UserServiceClient:
    def __init__(self, base_url):
        self._baseurl = base_url

    def get_user_data(self, user_id):
        params = {'user_id': user_id}
        res = requests.get(self._baseurl + "/data", params=params)
        print("User client response status: ", res.status_code, file=sys.stderr)
        try:
            decoded_res = res.json()
            if 'error' not in decoded_res:
                return decoded_res
            else:
                return None
        except ValueError:
            return None


class UserService(Flask):
    key = 'user_id'
    data_keys = ('gender', 'age')

    def __init__(self, data_source: AbstractDataSource):
        self._data = {}
        data = data_source.read_data()
        for row in data:
            assert row[self.key] not in self._data, f'Key value {self.key}={row[self.key]} is not unique in self._data'
            self._data[row[self.key]] = {k: row[k] for k in self.data_keys}

        super().__init__('users')

        urls = [
            ('/data', self.get_data, {}),
        ]

        for url in urls:
            if len(url) == 3:
                self.add_url_rule(url[0], url[1].__name__, url[1], **url[2])

    @staticmethod
    def _wrap_resp(user_data):
        if user_data is None:
            return {"error": "can't find such user"}
        return user_data

    def get_user_data(self, user_id):
        return self._data.get(user_id)

    def get_data(self):
        user_id = request.args.get('user_id')
        try:
            int_id = int(user_id)
        except ValueError:
            return {"error": "can't convert such user id to int"}
        print("Got user id: ", user_id, file=sys.stderr)
        return self._wrap_resp(self.get_user_data(int_id))

    def run_server(self, **kwargs):
        super().run(host='0.0.0.0', port=2001, **kwargs)