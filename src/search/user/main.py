from service import UserService
from common.settings import USER_DATA_FILE
from common.data_source import CSV


def main():
    users_server = UserService(CSV(USER_DATA_FILE))
    users_server.run_server(debug=True)


if __name__ == "__main__":
    main()
