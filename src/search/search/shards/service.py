
import pandas as pd


from typing import List
from search.simple.service import SimpleSearchServiceClient, SimpleSearchService
import sys
from flask import Flask, request
import json


class SearchInShardsServiceClient(SimpleSearchServiceClient):
    def __init__(self, baseurl):
        super().__init__(baseurl)


class SearchInShardsService(SimpleSearchService):

    def __init__(self, shards: List[SimpleSearchServiceClient]):
        Flask.__init__(self, 'shards-search')
        self._shards = shards
        self._DOCS_COLUMNS = ['document', 'key', 'key_md5']

        urls = [
            ('/search', self.search, {}),
        ]

        for url in urls:
            if len(url) == 3:
                self.add_url_rule(url[0], url[1].__name__, url[1], **url[2])

    def search(self):
        search_text = request.args.get("text")
        user_data_encoded = request.args.get("user_data")
        user_data = json.loads(user_data_encoded)
        geo_data_encoded = request.args.get("geo_data")
        geo_data = json.loads(geo_data_encoded)
        limit_str = request.args.get("limit")
        if limit_str == "":
            limit_str = "10"
        try:
            limit = int(limit_str)
        except ValueError:
            return {"error": "can't convert limit"}
        return self._wrap_resp(self.get_search_data(search_text, user_data, geo_data, limit))

    def get_search_data(self, *args, **kwargs) -> pd.DataFrame:
        shards_responses = []
        for shard in self._shards:
            shards_responses.append(shard.get_search_data(*args, **kwargs))
        print("Search in shard get search data", file=sys.stderr)
        self._data = pd.concat(shards_responses)  # possible data race in case of multi thread/async usage
        self._data.reset_index(inplace=True, drop=True)
        assert self._data.index.is_unique
        return super().get_search_data(*args, **kwargs)
