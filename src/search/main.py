from common.data_source import CSV
from geo.service import GeoServiceClient
from metasearch.http import Server
from metasearch.service import MetaSearchService

from search.shards.service import SearchInShardsServiceClient
from user.service import UserServiceClient


def main():
    user_service_client = UserServiceClient('http://users:2001')
    geo_service_client = GeoServiceClient('http://geo:2000')
    search_in_shards_client = SearchInShardsServiceClient('http://shards-search:4000')
    metasearch = MetaSearchService(search_in_shards_client, user_service_client, geo_service_client)
    server = Server('metasearch', metasearch=metasearch)
    server.run_server(debug=True)


if __name__ == '__main__':
    main()
