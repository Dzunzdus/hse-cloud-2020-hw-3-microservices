from search.simple.service import SimpleSearchService
import os
from common.settings import SEARCH_DOCUMENTS_DATA_FILES
from common.data_source import CSV


def main():
    shard_num = int(os.environ['SHARD_NUM'])
    print("SHARD_NUM: ", shard_num)
    shard_filepath = SEARCH_DOCUMENTS_DATA_FILES[shard_num - 1]

    simple_search = SimpleSearchService(CSV(shard_filepath))
    port = 3000 + shard_num
    simple_search.run_server(port, debug=True)


if __name__ == "__main__":
    main()
