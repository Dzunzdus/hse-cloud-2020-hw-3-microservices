from flask import Flask, request

import ipaddress as ip
import requests


import pandas as pd


class GeoServiceClient:
    def __init__(self, base_url):
        self._baseurl = base_url

    def get_geo_data(self, addr):
        params = {'ip_addr': addr}
        res = requests.get(self._baseurl + "/data", params=params)

        try:
            decoded_res = res.json()
            region = decoded_res['region']
            if region == '':
                return None
        except ValueError:
            return None
        return decoded_res


class GeoService(Flask):
    _data: pd.DataFrame
    _networks: pd.Series

    def __init__(self, data_source):

        super().__init__('geo')
        self._data = data_source.read_data(to_dict=False)
        for col in ('network', 'country_name'):
            assert col in self._data.columns
        self._networks = self._data['network'].apply(ip.ip_network)

        urls = [
            ('/data', self.get_data, {}),
        ]
        for url in urls:
            if len(url) == 3:
                self.add_url_rule(url[0], url[1].__name__, url[1], **url[2])

    def get_geo_data(self, addr):
        try:
            addr = ip.ip_address(addr)
        except ValueError:
            return None
        addr_in_net = self._networks.apply(lambda x: addr in x)
        addr_in_net = addr_in_net[addr_in_net == True]
        if len(addr_in_net) > 0:
            return {'region': self._data.loc[addr_in_net.head(1).index].iloc[0].country_name}
        return None

    def get_data(self):
        ip_addr = request.args.get('ip_addr')
        try:
            res = self.get_geo_data(ip_addr)
        except ValueError:
            return {'error': "Can't parse ip addr"}
        return res

    def run_server(self, **kwargs):
        super().run(host='0.0.0.0', port=2000, **kwargs)