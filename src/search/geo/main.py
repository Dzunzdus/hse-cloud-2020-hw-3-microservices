from service import GeoService
from common.data_source import CSV
from common.settings import GEO_DATA_FILE


def main():
    geo_server = GeoService(CSV(GEO_DATA_FILE))
    geo_server.run_server(debug=True)


if __name__ == "__main__":
    main()
